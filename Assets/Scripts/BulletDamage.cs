using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D coll) {

        if (coll.gameObject.tag == "Ship"){
            Destroy(coll.gameObject);
        }

        // Destroy Bullet in any case
        Destroy(gameObject);
        
    }
    

}
